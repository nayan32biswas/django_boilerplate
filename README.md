# Django Boilerplate

Cookiecutter boilerplate for django, docker-compose and gitlab CI.

## Usage

1. Install Cookiecutter.

```bash
pip install cookiecutter
```

2. Clone this project

```bash
git clone git@gitlab.com:nayan32biswas/django_boilerplate.git
```

3. Generate Project using this **Project**.

```bash
cookiecutter ./django_boilerplate/
```
